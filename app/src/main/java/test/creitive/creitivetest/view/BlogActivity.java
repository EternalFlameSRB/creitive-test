package test.creitive.creitivetest.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.creitive.creitivetest.R;
import test.creitive.creitivetest.data.Blog;
import test.creitive.creitivetest.data.BlogDetail;
import test.creitive.creitivetest.data.Constants;
import test.creitive.creitivetest.presenter.BlogListAdapter;
import test.creitive.creitivetest.data.RetrofitService;
import test.creitive.creitivetest.data.ServiceGenerator;
import test.creitive.creitivetest.util.ErrorHandling;
import test.creitive.creitivetest.util.SharedPreferencesManager;
import test.creitive.creitivetest.util.ViewDialog;

public class BlogActivity extends BaseActivity {

    private RetrofitService retrofitService;
    private BlogListAdapter sAdapter;
    private List<Blog> blogList;
    private String token_shared;
    private String blog_response;
    TextView tv_offline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);
        tv_offline = findViewById(R.id.tv_offline);

        if(!isOnline()){
            tv_offline.setVisibility(View.VISIBLE);
            return;
        } else {
            tv_offline.setVisibility(View.GONE);
        }
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        blogList = new ArrayList<>();

        sAdapter = new BlogListAdapter(blogList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(sAdapter);

        retrofitService = ServiceGenerator.createService(RetrofitService.class, "user", "password");

        token_shared = new SharedPreferencesManager(this).retrieveString(Constants.TOKEN, "");
        getBlogs(token_shared);

        sAdapter.setOnItemClickListener(new BlogListAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                int blog_id = blogList.get(position).getId();
                if (!isOnline()) {
                    ViewDialog.showDialog(BlogActivity.this, getString(R.string.no_internet));
                } else {
                    openBlogDetail(blog_id);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!isOnline()){
            tv_offline.setVisibility(View.VISIBLE);
        } else {
            tv_offline.setVisibility(View.GONE);
        }
    }

    private void getBlogs(String token) {

        Call<List<Blog>> call = retrofitService.getBlogList(token);
        call.enqueue(new Callback<List<Blog>>() {
            @Override
            public void onResponse(@NonNull Call<List<Blog>> call, @NonNull Response<List<Blog>> response) {
                if (response.isSuccessful()) {
                    blogList = response.body();
                    if (blogList != null) {
                        sAdapter.setItems(blogList);
                        sAdapter.notifyDataSetChanged();
                    }

                } else {
                    ErrorHandling.errorHandling(response.code(), getApplicationContext());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Blog>> call, @NonNull Throwable t) {
                Log.e("Error", t.getMessage());
            }
        });
    }

    private void openBlogDetail(int id) {
        Call<BlogDetail> call = retrofitService.getBlogDetail(token_shared, id);
        call.enqueue(new Callback<BlogDetail>() {
            @Override
            public void onResponse(@NonNull Call<BlogDetail> call, @NonNull Response<BlogDetail> response) {
                if (response.isSuccessful()) {
                    BlogDetail blogDetail = response.body();
                    if (blogDetail != null) {
                        blog_response = blogDetail.getBlogDetail();
                    }
                    new SharedPreferencesManager(getApplicationContext()).storeString(blog_response, "");
                    Intent intent = new Intent(BlogActivity.this, BlogDetailActivity.class);
                    intent.putExtra("blog_response", blog_response);
                    startActivity(intent);

                } else {
                    ErrorHandling.errorHandling(response.code(), getApplicationContext());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BlogDetail> call, @NonNull Throwable t) {
                Log.e("Error", t.getMessage());
            }
        });
    }

}
