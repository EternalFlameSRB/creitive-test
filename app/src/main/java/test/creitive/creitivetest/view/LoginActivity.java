package test.creitive.creitivetest.view;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.creitive.creitivetest.R;
import test.creitive.creitivetest.data.Constants;
import test.creitive.creitivetest.data.LoginRequest;
import test.creitive.creitivetest.data.RetrofitService;
import test.creitive.creitivetest.data.ServiceGenerator;
import test.creitive.creitivetest.data.TokenResponse;
import test.creitive.creitivetest.util.ErrorHandling;
import test.creitive.creitivetest.util.SharedPreferencesManager;
import test.creitive.creitivetest.util.ViewDialog;

public class LoginActivity extends BaseActivity {

    RetrofitService retrofitService;
    RelativeLayout relativeLayout;
    EditText edit_email;
    EditText edit_password;
    String string_email;
    String string_password;
    String string_token;
    String token_shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        relativeLayout = findViewById(R.id.relativeLayout);

        token_shared = new SharedPreferencesManager(this).retrieveString(Constants.TOKEN, "");
        if (token_shared.length() > 0) {
            startBlogActivity();
        }
    }

    public static boolean isEmailValid(String email) {
        return !(email == null || TextUtils.isEmpty(email)) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void onClickLogin(View V) {
        edit_email = findViewById(R.id.edit_email);
        edit_password = findViewById(R.id.edit_password);

        string_email = edit_email.getText().toString();
        string_password = edit_password.getText().toString();

        if (!isEmailValid(string_email)) {
            edit_email.setError(getString(R.string.error_email));
            return;
        }
        if (string_password.length() < 6) {
            edit_password.setError(getString(R.string.error_minimal_length));
            return;
        }
        if (!isOnline()) {
            ViewDialog.showDialog(this, getString(R.string.no_internet));
        } else {
            attemptLogin(string_email, string_password);
        }
    }


    public void attemptLogin(String username, String password) {
        retrofitService = ServiceGenerator.createService(RetrofitService.class, "user", "password");
        Call<TokenResponse> call = retrofitService.basicLogin(new LoginRequest(username, password));
        call.enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<TokenResponse> call, @NonNull Response<TokenResponse> response) {
                if (response.isSuccessful()) {
                    TokenResponse tokenResponse = response.body();
                    if (tokenResponse != null) {
                        string_token = tokenResponse.getToken();
                    }
                    new SharedPreferencesManager(getApplicationContext()).storeString(Constants.TOKEN, string_token);
                    startBlogActivity();
                } else {
                    ErrorHandling.errorHandling(response.code(), getApplicationContext());
                }
            }

            @Override
            public void onFailure(@NonNull Call<TokenResponse> call, @NonNull Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void startBlogActivity() {
        Intent intent = new Intent(LoginActivity.this, BlogActivity.class);
        startActivity(intent);
        finish();
    }


}
