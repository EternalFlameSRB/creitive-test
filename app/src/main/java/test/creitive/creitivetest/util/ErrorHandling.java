package test.creitive.creitivetest.util;

import android.content.Context;
import android.widget.Toast;

public class ErrorHandling {

    public static void errorHandling(int code, Context context) {
        switch (code) {
            case 400:
                Toast.makeText(context, "Bad request", Toast.LENGTH_SHORT).show();
                break;
            case 401:
                Toast.makeText(context, "Not authorised", Toast.LENGTH_SHORT).show();
                break;
            case 403:
                Toast.makeText(context, "Forbidden", Toast.LENGTH_SHORT).show();
                break;
            case 404:
                Toast.makeText(context, "Not found", Toast.LENGTH_SHORT).show();
                break;
            case 406:
                Toast.makeText(context, "Not acceptable", Toast.LENGTH_SHORT).show();
                break;
            case 415:
                Toast.makeText(context, "Unsupported type", Toast.LENGTH_SHORT).show();
                break;
            case 503:
                Toast.makeText(context, "Aervice not available", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(context, "Unknown error", Toast.LENGTH_SHORT).show();
                break;
        }
    }

}
