package test.creitive.creitivetest.presenter;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import test.creitive.creitivetest.R;
import test.creitive.creitivetest.data.Blog;

/**
 * Created by EternalFlame on 8/23/2016.
 * Bla
 */
public class BlogListAdapter extends RecyclerView.Adapter<BlogListAdapter.MyViewHolder> {

    private List<Blog> blogList;
    private static ClickListener clickListener;

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView blog_title;
        private ImageView blog_image;
        private TextView blog_description;

        private MyViewHolder(View view) {
            super(view);
            blog_title = view.findViewById(R.id.blog_title);
            blog_image = view.findViewById(R.id.blog_image);
            blog_description = view.findViewById(R.id.blog_description);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public BlogListAdapter(List<Blog> blogList) {
        this.blogList = blogList;
    }

    public void setItems(List<Blog> blogList) {
        this.blogList = blogList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.blog_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Blog blog = blogList.get(position);
        holder.blog_title.setText(blog.getTitle());
        if(blog.getImageUrl()!=null && !blog.getImageUrl().equals("null") && blog.getImageUrl().length() > 0) {
            holder.blog_image.setVisibility(View.VISIBLE);
            Picasso.with(holder.blog_image.getContext()).load(blog.getImageUrl()).into(holder.blog_image);
        }
        holder.blog_description.setText(fromHtml(blog.getDescription()));
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        BlogListAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    @Override
    public int getItemCount() {
        return blogList.size();
    }

    public Spanned fromHtml(String html){
        Spanned result;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }


}