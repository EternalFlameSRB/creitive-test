package test.creitive.creitivetest.data;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RetrofitService {
    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"
    })

    @POST("login")
    Call<TokenResponse> basicLogin(@Body LoginRequest loginRequest);

    @Headers({
            "Accept: application/json"
    })
    @GET("blogs")
    Call<List<Blog>> getBlogList(@Header("X-Authorize") String token);

    @Headers({
            "Accept: application/json"
    })
    @GET("blogs/{id}")
    Call<BlogDetail> getBlogDetail(@Header("X-Authorize") String token,
                                   @Path ("id") Integer id);
}




