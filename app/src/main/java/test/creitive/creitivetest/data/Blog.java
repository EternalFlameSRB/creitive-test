package test.creitive.creitivetest.data;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Blog {

    @Expose
    private int id;
    @Expose
    private String title;
    @Expose
    @SerializedName("image_url")
    private String imageUrl;
    @Expose
    private String description;

    /**
     * No args constructor for use in serialization
     *
     */
    public Blog() {
    }

    /**
     *
     * @param id
     * @param title
     * @param imageUrl
     * @param description
     */
    public Blog(int id, String title, @Nullable String imageUrl, String description) {
        super();
        this.id = id;
        this.title = title;
        this.imageUrl = imageUrl;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
