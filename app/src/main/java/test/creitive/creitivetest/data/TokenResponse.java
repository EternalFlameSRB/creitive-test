package test.creitive.creitivetest.data;

import com.google.gson.annotations.Expose;

public class TokenResponse {

    @Expose
    private String token;

    public TokenResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
