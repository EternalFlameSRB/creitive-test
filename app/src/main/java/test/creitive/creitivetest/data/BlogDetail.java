package test.creitive.creitivetest.data;

import com.google.gson.annotations.Expose;

public class BlogDetail {

    @Expose
    private String content;

    public BlogDetail(String content) {
        this.content = content;
    }

    public String getBlogDetail() {
        return content;
    }

    public void setBlogDetail(String content) {
        this.content = content;
    }
}
